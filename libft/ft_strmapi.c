/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ahammou- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 17:11:21 by ahammou-          #+#    #+#             */
/*   Updated: 2019/01/07 17:11:23 by ahammou-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char			*dst;
	unsigned int	i;
	unsigned int	j;

	if (!s || !f)
		return (NULL);
	i = 0;
	j = (unsigned int)ft_strlen((char *)s);
	if (!(dst = ft_strnew(j)))
		return (NULL);
	while (i != j)
	{
		dst[i] = f(i, s[i]);
		i++;
	}
	return (dst);
}
