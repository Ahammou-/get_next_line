/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ahammou- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 17:11:08 by ahammou-          #+#    #+#             */
/*   Updated: 2019/01/07 17:11:10 by ahammou-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*dst;
	int		i;
	int		j;

	if (!s || !f)
		return (NULL);
	i = 0;
	j = ft_strlen((char *)s);
	if (!(dst = ft_strnew(j)))
		return (NULL);
	while (i != j)
	{
		dst[i] = f(s[i]);
		i++;
	}
	return (dst);
}
